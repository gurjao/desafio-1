package com.stefanini.testestefanini.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.stefanini.testestefanini.model.Task;

/**
 * MainController
 */
@RestController
public class MainController {
	public Map<Integer, Task> tasks = new HashMap<>();
   
	@GetMapping(value="/")
	public String primeiroMetodo() {
		return "Teste Stefanini!";
	}

	@RequestMapping(value = "/addTask")
	public ResponseEntity<Task> adicionar(@RequestParam("id") Integer id, @RequestParam("nome") String nome) {
		
		Task task = new Task(id, nome);
		tasks.put(id, task);

		System.out.println("Adicionado");
		return new ResponseEntity<Task>(task, HttpStatus.OK);
	}

	@RequestMapping(value = "/listTasks", method = RequestMethod.GET)
	public ResponseEntity<List<Task>> listar() {
		System.out.println("Listar");
		return new ResponseEntity<List<Task>>(new ArrayList<Task>(tasks.values()), HttpStatus.OK);
	  }
	  
	@RequestMapping(value = "/searchTask/{id}", method = RequestMethod.GET)
	public ResponseEntity<Task> buscar(@PathVariable("id") Integer id) {
		Task task = tasks.get(id);

		if (task == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Task>(task, HttpStatus.OK);
	}

	@RequestMapping(path = "/deleteTask/{id}")
	public ResponseEntity<?> deletar(@PathVariable("id") Integer id) {
		Task task = tasks.remove(id);

		if (task == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		System.out.println("Excluído");
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(path = "/deleteTasks")
	public ResponseEntity<Task> deletarTudo() {
		Map<Integer, Task> tasks = new HashMap<>();

		System.out.println("Tudo excluído");
		return new ResponseEntity<Task>(HttpStatus.OK);
	}

}